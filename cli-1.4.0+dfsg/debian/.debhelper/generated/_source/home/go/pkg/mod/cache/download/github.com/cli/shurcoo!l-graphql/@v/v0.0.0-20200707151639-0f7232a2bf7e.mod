module github.com/shurcooL/graphql

go 1.13

require (
	github.com/graph-gophers/graphql-go v0.0.0-20200622220639-c1d9693c95a6
	golang.org/x/net v0.0.0-20200707034311-ab3426394381
)
