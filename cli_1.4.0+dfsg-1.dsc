Format: 3.0 (quilt)
Source: cli
Binary: gh
Architecture: any
Version: 1.4.0+dfsg-1
Maintainer: Joao Paulo Lima de Oliveira <jlima.oliveira11@gmail.com>
Homepage: https://cli.github.com/
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/jpaulo/gh
Vcs-Git: https://salsa.debian.org/jpaulo/gh.git
Build-Depends: debhelper-compat (= 13), golang-go, git, dh-golang, dirmngr, software-properties-common
Package-List:
 gh deb misc optional arch=any
Checksums-Sha1:
 823e88cf62863b0c150c648b0a812bc086b4f517 208512 cli_1.4.0+dfsg.orig.tar.xz
 4c75eb83b2faa2d77928376bfe4268434d950b3e 2608 cli_1.4.0+dfsg-1.debian.tar.xz
Checksums-Sha256:
 3769706f5d61c269e008e7efbe138070a50e1f99faca0855ad58e5538312ddd4 208512 cli_1.4.0+dfsg.orig.tar.xz
 ce6a760f641c37c4badb1f7dbbdbb754b43bbe62145a72bcdff353a4ba938260 2608 cli_1.4.0+dfsg-1.debian.tar.xz
Files:
 7e6dc15662d34578ff1f5e232299553c 208512 cli_1.4.0+dfsg.orig.tar.xz
 8515f14627afcf81e801406f73c0fe16 2608 cli_1.4.0+dfsg-1.debian.tar.xz
